/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.GridPane;

/**
 *
 * @author CltControl
 */
public class Computadora extends Jugador{
    private transient GridPane gridpane;
    public Computadora(String nombre){
        super();
        setNombre(nombre);
    }

    public GridPane getGridpane() {
        return gridpane;
    }

    public void setGridpane(GridPane gridpane) {
        this.gridpane = gridpane;
    }
    

    @Override
    public String toString() {
        return "Computadora{"+ getNombre()+","+ getTabla().getBaraja()+'}';
    }   
}
