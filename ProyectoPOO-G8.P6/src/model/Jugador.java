/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author CltControl
 */
public class Jugador implements Serializable{
    private String nombre;
    private Tabla tabla;
    
    public Jugador(){
        tabla = new Tabla();
    }

    public Jugador(String nombre, Tabla tabla) {
        this.nombre = nombre;
        tabla = new Tabla();
    }
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Tabla getTabla() {
        return tabla;
    }

    public void setTabla(Tabla tabla) {
        this.tabla = tabla;
    }

    @Override
    public String toString() {
        return "Jugador{" + "nombre=" + nombre + ", tabla=" + tabla + '}';
    }
    
    
}
