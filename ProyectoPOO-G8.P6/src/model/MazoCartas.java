/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author laptop
 */
public class MazoCartas implements Serializable{
    private ArrayList<Carta> baraja;
    private int numeroDeCartas;
    private SortedMap<Carta,Boolean> mapCartas;
    public MazoCartas(){
        baraja = new ArrayList<Carta>();
        mapCartas = new TreeMap<Carta,Boolean>();
    }
    public MazoCartas(ArrayList<Carta> baraja) {
        this.baraja = baraja;
        numeroDeCartas = baraja.size();
    }

    public ArrayList<Carta> getBaraja() {
        return baraja;
    }

    public void setBaraja(ArrayList<Carta> baraja) {
        this.baraja = baraja;
    }

    public int getNumeroDeCartas() {
        return numeroDeCartas;
    }

    public void setNumeroDeCartas(int numeroDeCartas) {
        this.numeroDeCartas = numeroDeCartas;
    }

    public SortedMap<Carta, Boolean> getMapCartas() {
        return mapCartas;
    }

    @Override
    public String toString() {
        return "MazoCartas{" + "baraja=" + baraja + ", numeroDeCartas=" + numeroDeCartas + '}';
    }
    @Override
    public boolean equals(Object o){
        if(o == null){return false;}
        if(getClass()!=o.getClass()){return false;}
        final MazoCartas other = (MazoCartas) o;
        if(this.baraja!= other.baraja){return false;}
        return true;  
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.baraja);
        hash = 17 * hash + this.numeroDeCartas;
        hash = 17 * hash + Objects.hashCode(this.mapCartas);
        return hash;
    }
    
    
    
}
