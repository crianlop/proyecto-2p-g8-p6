/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author CltControl
 */
public class Tabla implements Serializable{
    private Carta[][] tablacartas;
    private MazoCartas baraja;
    public Tabla(){
        baraja = new MazoCartas();
        tablacartas = new Carta[4][4];
    }

    public MazoCartas getBaraja() {
        return baraja;
    }

    public void setBaraja(MazoCartas baraja) {
        this.baraja = baraja;
    }

    public Carta[][] getTablacartas() {
        return tablacartas;
    }

    public void setTablacartas(Carta[][] tablacartas) {
        this.tablacartas = tablacartas;
    }

    @Override
    public String toString() {
        return "Tabla{" + "tablacartas=" + tablacartas + ", baraja=" + baraja + '}';
    }
    

 
}
