/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoPOOg8p6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.Thread.State;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import model.Carta;
import model.Jugador;
import model.TipoJuego;
import threads.Juego;
import views.ViewVentanaPrincipal;

/**
 *
 * @author laptop
 */
public class Main extends Application {
    ArrayList<Juego> juegos;

    
    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {

        Juego juego = new Juego();
        juego.setJugador(new Jugador());
        ViewVentanaPrincipal ventanaPrincipal = new ViewVentanaPrincipal(juego);
        Scene scene = new Scene(ViewVentanaPrincipal.getRoot(),640,480);
        File f = new File("src/css/estilos.css");
        scene.getStylesheets().add("file:///"+f.getAbsolutePath().replace("\\", "/"));
        primaryStage.setTitle("Proyecto Segundo Parcial P6 G8");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        launch(args);
        
    }
}
