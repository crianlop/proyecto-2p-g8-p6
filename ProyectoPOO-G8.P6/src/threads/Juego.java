/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.*;
import views.ViewNuevoJuego;
/**
 *
 * @author laptop
 */

public class Juego implements Comparable<Juego>,Serializable{//Clase juego que implementa a la clase comparable
    private Date fecha;//atributos
    private MazoCartas baraja;
    private TipoJuego tipojuego;
    private Jugador jugador;
    private transient ArrayList<Computadora> computadoras;
    private int nrocomputadora; 
    private Carta cartaEnJuego;
    private boolean juegoTerminado;
    private Jugador ganador;
    private int duracion;
    private ArrayList<Juego> juegos;
    private String nombre;
    private boolean empezoJuego;
    private boolean cartasRepartidas;
    private boolean computadorasVisibles;
    /* Constructor del juego, este setea el juegoTerminado como false, crea la clase Jugador(vacia), la lista de computadoras(vacia), genera el generador de aleatorios
    escoge ademas un TipoJuego aleatoriamente, crea una baraja MazoCartas vacia, consigue la fecha de hoy, y utiliza el metodo cargarCartas() para llenar las cartas.
    */
    public Juego(){//metodo de juego
        juegos = new ArrayList<Juego>();//creacion de objeto de tipo arraylist
        juegoTerminado = false;//empieza en falso para que el juego pueda correr 
        jugador = new Jugador();
        computadoras = new ArrayList<Computadora>();
        Random generator = new Random();
        int randomIndex = generator.nextInt(TipoJuego.values().length);
        tipojuego = TipoJuego.values()[randomIndex];
        baraja = new MazoCartas();
        empezoJuego = false;
        cartasRepartidas = false;
        computadorasVisibles = true;
        cargarCartas();    
        cargarJuegos();
        nrocomputadora = 1; // este numero de aqui se tiene que cambiar, no se como
    }
    public Juego(Jugador jugador, ArrayList<Computadora> computadoras,boolean computadorasVisibles, int nrocomputadora){//Constructor de la clase jugador
        juegos = new ArrayList<Juego>();
        juegoTerminado = false;
        this.jugador = jugador;
        this.computadoras = computadoras;
        Random generator = new Random();
        int randomIndex = generator.nextInt(TipoJuego.values().length);
        tipojuego = TipoJuego.values()[randomIndex];
        baraja = new MazoCartas();
        empezoJuego = false;
        cartasRepartidas = false;
        this.computadorasVisibles = computadorasVisibles;
        cargarCartas();    
        cargarJuegos();
        this.nrocomputadora = nrocomputadora; // este numero de aqui se tiene que cambiar, no se como
    }
    public Juego(TipoJuego tipojuego){
        juegos = new ArrayList<Juego>();
        juegoTerminado = false;
        jugador = new Jugador();
        computadoras = new ArrayList<Computadora>();
        Random generator = new Random();
        this.tipojuego = tipojuego;
        baraja = new MazoCartas();
        empezoJuego = false;
        cartasRepartidas = false;
        computadorasVisibles = true;
        cargarCartas();    
        cargarJuegos();
        nrocomputadora = 1; // este numero de aqui se tiene que cambiar, no se como
    }
    
    // empieza el juego. Llama al metodo crearComputadoras, reparte las cartas al jugador, y empieza el thread de Juego
    public void empezarJuego(ImageView imv,Juego juego,ArrayList<Stage> stages,GridPane tablero){//Creacion de la tabla llamando
        fecha = new Date();
        
        crearComputadoras();//Metodo que crea a la computadora 
        repartirCartas(jugador);
       
    }
   /* El thread principal. Apenas comienza el juego, se duerme por 10s para darle tiempo al jugador ver sus cartas. Despues de esto, entra en un bucle, hasta que
    el juego se acabe. En el bucle, se escoge una carta aleatoriamente, y se la setea como la cartaEnJuego (SOLO SI la carta ya no ha sido llamada antes,
    verificando esto con el mapCartas), la cual es la que se usara en las verificaciones de
    frijoles. Ademas, por cada computadora, crea y empieza el threadComputadora, con parametros las computadoras de la lista, la carta en juego, y si el juego
    ya se acabo. Despues de esto, se duerme por 10s, y el bucle se repite.
    */
 
       
       
   
   // metodo basico de carga de archivos, lee del csv la informacion de las cartas, y las agrega a la baraja y a la coleccion de cartas, con valor false.
    public void cargarCartas(){
        try{
            BufferedReader bfreader = new BufferedReader(new FileReader("src/data/cartasloteria.csv"));
            String line;
            while((line = bfreader.readLine()) != null){
                String[] cartaString = line.split(",");
                Carta carta = new Carta(Integer.parseInt(cartaString[0]),cartaString[1]);
                baraja.getBaraja().add(carta);
                baraja.getMapCartas().put(carta, false);
            }
            baraja.setNumeroDeCartas(baraja.getBaraja().size());
            bfreader.close();
        }catch(IOException ioe){
            System.out.println(ioe.getMessage());
        }
    }
    
    // crea las computadoras del juego, dependiendo del nroComputadora puesto por el usuario.
    public void crearComputadoras(){
        for(int i=0;i<nrocomputadora;i++){
            Computadora computadora = new Computadora("Computadora "+(i+1));
            repartirCartas(computadora);
            computadoras.add(computadora);
        }
    }
    
    // reparte aleatoriamente cartas al jugador ingresado como parametro, agregandala a la baraja de su MazoCartas
    // y las coloca en la matriz de cartas para la 
    public void repartirCartas(Jugador j){

        while(j.getTabla().getBaraja().getBaraja().size()<16){
            Random generator = new Random();
            int randomIndex = generator.nextInt(baraja.getBaraja().size());
            Carta carta = baraja.getBaraja().get(randomIndex);
            if(!j.getTabla().getBaraja().getBaraja().contains(carta)){
            j.getTabla().getBaraja().getBaraja().add(carta);
            j.getTabla().getBaraja().getMapCartas().put(carta, false);
            }
        }
        int i=0;
        for(int fila=0;fila<4;fila++){
            for(int columna=0;columna<4;columna++){
                j.getTabla().getTablacartas()[fila][columna]=j.getTabla().getBaraja().getBaraja().get(i);
                i++;
            }
        }
        j.getTabla().getBaraja().setNumeroDeCartas(j.getTabla().getBaraja().getBaraja().size());
    }

    // setters, getters, toString
    public MazoCartas getBaraja() {
        return baraja;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TipoJuego getTipojuego() {
        return tipojuego;
    }

    public boolean isEmpezoJuego() {
        return empezoJuego;
    }

    public void setEmpezoJuego(boolean empezoJuego) {
        this.empezoJuego = empezoJuego;
    }

    public boolean isCartasRepartidas() {
        return cartasRepartidas;
    }

    public void setCartasRepartidas(boolean cartasRepartidas) {
        this.cartasRepartidas = cartasRepartidas;
    }
    

    public void setTipojuego(TipoJuego tipojuego) {
        this.tipojuego = tipojuego;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public ArrayList<Computadora> getComputadoras() {
        return computadoras;
    }

    public void setComputadoras(ArrayList<Computadora> computadoras) {
        this.computadoras = computadoras;
    }

    public int getnrocomputadora() {
        return nrocomputadora;
    }

    public void setnrocomputadora(int nroComputadora) {
        this.nrocomputadora = nroComputadora;
    }

    public Carta getCartaEnJuego() {
        return cartaEnJuego;
    }

    public void setCartaEnJuego(Carta cartaEnJuego) {
        this.cartaEnJuego = cartaEnJuego;
    }

    public boolean isJuegoTerminado() {
        return juegoTerminado;
    }

    public void setJuegoTerminado(boolean juegoTerminado) {
        this.juegoTerminado = juegoTerminado;
    }

    
    public String toString() {
        return "Juego{" + "fecha=" + fecha +  ", tipojuego=" + tipojuego + ", jugador=" + jugador + ", computadoras=" + computadoras + ", nroComputadora=" + nrocomputadora + '}';
    }

    public int getNrocomputadora() {
        return nrocomputadora;
    }

    public void setNrocomputadora(int nroComputadora) {
        this.nrocomputadora = nroComputadora;
    }

    public Jugador getGanador() {
        return ganador;
    }

    public void setGanador(Jugador ganador) {
        this.ganador = ganador;
    }

    public ArrayList<Juego> getJuegos() {
        return juegos;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public boolean isComputadorasVisibles() {
        return computadorasVisibles;
    }

    public void setComputadorasVisibles(boolean computadorasVisibles) {
        this.computadorasVisibles = computadorasVisibles;
    }
    

    public int getDuracion() {
        return duracion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    

   
    
    public int compareTo(Juego o) {
        return fecha.compareTo(o.getFecha()); 
    }
        public void actualizarArchivoJuegos(){
        FileOutputStream fout = null;
        try{
            fout = new FileOutputStream("juegos.ser");
            ObjectOutputStream out = new ObjectOutputStream(fout);
            out.writeObject(juegos);
            out.flush();
            fout.close();   
        }
        catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        finally{
            try{
                fout.close();     
            }
            catch(IOException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    
        private void cargarJuegos(){
        juegos = new ArrayList<>();
        Path path = Paths.get("juegos.ser");
        if (Files.exists(path)){
            ObjectInputStream in = null;
            try{
                in = new ObjectInputStream(new FileInputStream("juegos.ser"));
                juegos = (ArrayList<Juego>) in.readObject();
                in.close();
            }catch(FileNotFoundException ex){
                System.out.println(ex.getMessage());
            }
            catch(IOException ex){
                System.out.println(ex.getMessage());
            }
            catch(ClassNotFoundException ex){
                System.out.println(ex.getMessage());
            }
            finally{
                try{
                    in.close();
                }
                catch(IOException ex){
                    System.out.println(ex.getMessage());
                }
            }
        }
        ;
    } 
}

