/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import model.*;
/**
 *
 * @author CltControl
 */
public class ThreadComputadora extends Thread{
    private Computadora computadora;
    private Carta carta;
    private Juego juego;
// Thread con parametros para poder utilizarlos en el run
    public ThreadComputadora(Computadora computadora, Carta carta, Juego juego) {//Constructor de la clase
        this.computadora = computadora;
        this.carta = carta;
        this.juego = juego;
    }
    
// Metodo run que recibe la computadora, y la carta. Despues de un delay de 2s, si en la tabla de la computadora esta la carta ingresada, le pone el frijol.
// crea  y llama al thread juegoTerminado para verificar si el juego se acabo o no cada vez
    public void run(){
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadComputadora.class.getName()).log(Level.SEVERE, null, ex);
        }
        Carta[][] cartas = computadora.getTabla().getTablacartas();
        System.out.println(computadora.getGridpane()!= null);
        
        if(computadora.getGridpane()!=null){
        for(Node pane:computadora.getGridpane().getChildren()){
            if (cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)].getId() ==  juego.getCartaEnJuego().getId() && !computadora.getTabla().getBaraja().getMapCartas().get(cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)])) {
           try {
              ImageView frijol = new ImageView(new Image(new FileInputStream(new File("src/data/images/frejol.png"))));
              frijol.setFitHeight(50);
              frijol.setFitWidth(50);
              StackPane newPane = (StackPane) pane;
              computadora.getTabla().getBaraja().getMapCartas().replace(cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)], true);
              juegoTerminado thread = new juegoTerminado(juego,computadora);
              thread.start();
              Platform.runLater(()->{
              newPane.getChildren().add(frijol);});
             
          } catch (Exception ex) {
             System.out.println(ex.getMessage());
          }}
                } 
        }
         else{
          for(int col =0;col<4;col++){
              for(int fil =0;fil<4;fil++){
                  if(cartas[col][fil].getId() == juego.getCartaEnJuego().getId() && !computadora.getTabla().getBaraja().getMapCartas().get(cartas[col][fil])){
                  System.out.println("LA TIENE "+computadora.getNombre());
                  computadora.getTabla().getBaraja().getMapCartas().replace(cartas[col][fil],true);
                  juegoTerminado thread = new juegoTerminado(juego,computadora);
                  thread.start();
                  }
              }
          }
            }
    }   
    
}

