/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import com.sun.corba.se.impl.protocol.giopmsgheaders.Message;
import java.io.File;
import java.io.FileNotFoundException;
import static java.lang.Thread.sleep;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Computadora;
import views.ViewNuevoJuego;
import views.ViewVentanaPrincipal;

/**
 *
 * @author jarac
 */
public class ThreadGriton extends Thread {
    Juego juego;
    ImageView cartaEnJuego = new ImageView();
    ArrayList<Stage> referencia;
    GridPane gridpane;
    
    public ThreadGriton(ImageView imv, Juego juego,ArrayList<Stage> referencia,GridPane gridpane) {
        this.cartaEnJuego = imv;
        this.juego = juego; 
        this.referencia = referencia;
        this.gridpane = gridpane;
    }

     // thread del griton, el cual escoge una carta aleatoria y la pone como la "Carta en juego", ademas, este thread 
    // crea tambien el thread de computadora y el thread de verificar carta del jugador, para ver si es que en los tableros
    // de los jugadores esta la carta en juego. Este thread esta en un bucle, el cual solo se acaba cuando algun jugador
    // gane el juego.
    public void run() {//correr la pagina
        
          System.out.println(new Date());
       
            long now = System.currentTimeMillis();
            try {
            juego.setNombre(juego.getJugador().getNombre());
            sleep(10000);
                 } catch (Exception ex) {
                Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                }
        
        while (!juego.isJuegoTerminado()) {
       Random generator = new Random();
       int randomIndex = generator.nextInt(juego.getBaraja().getBaraja().size());
       if(!juego.getBaraja().getMapCartas().get(juego.getBaraja().getBaraja().get(randomIndex))){
       juego.setCartaEnJuego(juego.getBaraja().getBaraja().get(randomIndex));
       juego.getBaraja().getMapCartas().replace(juego.getCartaEnJuego(), true);
       juego.setEmpezoJuego(true);
       String ruta = "src/data/images/deck/" + juego.getCartaEnJuego().getId() + ".png"; // NO Se cae
       File cartaImg = new File(ruta);
       String ruta1 = null;
       try {
       ruta1 = cartaImg.toURI().toURL().toString();
        } catch (MalformedURLException e) {
         System.out.println(e.getMessage());
                }
        Image carta = new Image(ruta1, 130, 70, true, true);
        cartaEnJuego.setImage(carta);
        cartaEnJuego.setFitWidth(100);
        cartaEnJuego.setFitHeight(150);
       Pane referencia = (Pane) cartaEnJuego.getScene().getRoot();
              VerificarCarta threadJugador = new VerificarCarta(juego,juego.getJugador(),juego.getCartaEnJuego(),gridpane); 
       threadJugador.start();
       for(Computadora c:juego.getComputadoras()){
           ThreadComputadora threadC = new ThreadComputadora(c,juego.getCartaEnJuego(),juego);
           threadC.start();
       }
        try {
            sleep(4000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
       }    
       long finish = System.currentTimeMillis();
       Long y = ((finish-now)/1000)/60;
       juego.setDuracion(y.intValue());
       juego.getJuegos().add(juego);   
       juego.actualizarArchivoJuegos();
                
      Platform.runLater(()->{
        
      if(juego.getGanador().equals(juego.getJugador())){
      Alert alert = new Alert(Alert.AlertType.INFORMATION);
      alert.setTitle("GANADOR");
      alert.setHeaderText("GANASTE!");
      alert.setContentText("Te demoraste: "+juego.getDuracion());
      alert.showAndWait();
          try { 
               for(Stage s:referencia){
                  s.hide();          
              }
               sleep(0);
               
              ViewVentanaPrincipal nuevaVentana = new ViewVentanaPrincipal();
              cartaEnJuego.getScene().setRoot(nuevaVentana.getRoot());
          } catch (FileNotFoundException ex) {
                            System.out.println(ex.getMessage());
          } catch (InterruptedException ex) {
                            System.out.println(ex.getMessage());
          }
        }
       else{
             Alert alert = new Alert(Alert.AlertType.INFORMATION);
      
      alert.setTitle("PERDEDOR");
      alert.setHeaderText("PERDEDOR, PERDISTE! PERDISTE A "+juego.getGanador().getNombre());
      alert.setContentText("Te demoraste: "+juego.getDuracion());
      alert.showAndWait();
          try {
               for(Stage s:referencia){
                  s.hide();          
              }
               sleep(0);
              ViewVentanaPrincipal nuevaVentana = new ViewVentanaPrincipal();
              cartaEnJuego.getScene().setRoot(nuevaVentana.getRoot());
          } catch (FileNotFoundException ex) {
                            System.out.println(ex.getMessage());
          }      catch (InterruptedException ex) {
                                   System.out.println(ex.getMessage());
                 }
      
      }
        });  
    }
}
