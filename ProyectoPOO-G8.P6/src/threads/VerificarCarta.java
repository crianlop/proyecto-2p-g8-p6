/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import model.Carta;
import model.Computadora;
import model.Jugador;
import views.ViewNuevoJuego;

/**
 *
 * @author laptop
 */
public class VerificarCarta extends Thread {
    Juego juego;
    Jugador jugador;

    Carta cartaJ;
    GridPane tablero;

    public VerificarCarta(Juego juego, Jugador jugador, Carta cartaJ,GridPane tablero) {
        this.juego = juego;
        this.jugador = jugador;

        this.cartaJ = cartaJ;
        this.tablero = tablero;
    }
    // revisa el thread del jugador. Si es que alguna carta de su tablero es la misma de la carta en juego, entonces se colocara
    // una imagen para notificarle al jugador de que la imagen esta ahi, sin embargo no la marcara.
    public void run(){
        Carta[][] cartas = jugador.getTabla().getTablacartas();
              for(Node pane:tablero.getChildren()){
                  
            if (cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)].getId() ==  juego.getCartaEnJuego().getId() && !jugador.getTabla().getBaraja().getMapCartas().get(cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)])) {
           try {
              ImageView frijol = new ImageView(new Image(new FileInputStream(new File("src/data/images/alerta.jfif"))));
              frijol.setFitHeight(50);
              frijol.setFitWidth(50);
              StackPane newPane = (StackPane) pane;
              Platform.runLater(()->{
              newPane.getChildren().add(frijol);});
                  try {
              Thread.sleep(3000);
                  } catch (InterruptedException ex) {
                Logger.getLogger(VerificarCarta.class.getName()).log(Level.SEVERE, null, ex);
                  }
             Platform.runLater(()->{
              frijol.setImage(null);});
          } catch (Exception ex) {
             System.out.println(ex.getMessage());
          }
      } 
    }
    }
}


