/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import java.io.File;
import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 *
 * @author laptop
 */
public class mostrarMAL extends Thread{
    StackPane stackpane;

    public mostrarMAL(StackPane stackpane) {
        this.stackpane = stackpane;
    }
    // este thread lo llaman los imageviews de las cartas del jugador. Si es que el jugador pone una carta que no sea la carta
    //que este en juego ((Esta verificacion se hace en el boton)) se llama a este thread, el cual pone una equis encima del
    // imageview de la carta por 1 segundo.
    public void run(){
        try {
              ImageView equis= new ImageView(new Image(new FileInputStream(new File("src/data/images/equis.png"))));
              equis.setFitHeight(50);
              equis.setFitWidth(50);
              Platform.runLater(()->{
              stackpane.getChildren().add(equis);});
                  try {
              Thread.sleep(1000);
                  } catch (InterruptedException ex) {
                Logger.getLogger(VerificarCarta.class.getName()).log(Level.SEVERE, null, ex);
                  }
             Platform.runLater(()->{
              equis.setImage(null);});
          } catch (Exception ex) {
             System.out.println(ex.getMessage());
          }
        
    }
    
    
}
