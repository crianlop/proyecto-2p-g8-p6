/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import threads.Juego;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
/**
 *
 * @author Arq. Apolo
 */
public class ViewConfiguraciones {
    private static StackPane root = new StackPane();
    private Juego juego;

    public ViewConfiguraciones(Juego juego) {
        
        this.juego = juego;
        System.out.println(juego);
        
        try {
            createContent();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ViewConfiguraciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public static StackPane getRoot() {
        return root;
    }
    
    // ventana config, donde el jugador cambia si las computadoras se ven o no, y contra cuantos oponentes va.
    public void createContent() throws FileNotFoundException {


        root = new StackPane(); 
        Image image  = new Image(new FileInputStream("src/data/images/background.jpg"));
        ImageView fondo = new ImageView(image);
        fondo.setFitWidth(640);
        fondo.setFitHeight(480);
        Rectangle rectangle = new Rectangle(200,700, Color.YELLOW);
        rectangle.setArcWidth(20);
        rectangle.setArcHeight(20);
        Button btnGuardar = new Button("Guardar");
        Button btnRegresar = new Button("Regresar");
        
        VBox cntBotones = new VBox(15);
        

        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(1, 2));
       
        ChoiceBox cb1 = new ChoiceBox(FXCollections.observableArrayList("Visible","Oculta"));
        Label label = new Label("Configuraciones");
        Label label1=  new Label ("Cantidad de oponentes:");
        
        Label label2=  new Label ("Visibilidad de cartas:");

        
        
   
        cntBotones.setPadding(new Insets(20));
        cntBotones.setAlignment(Pos.CENTER);


        cntBotones.getChildren().addAll(label, label1,cb, label2,cb1,btnGuardar,btnRegresar);
   
        root.setPadding(new Insets(20));
        root.getChildren().addAll(fondo,rectangle,cntBotones);
        

        btnGuardar.setOnAction(e->{
            String value = (String) cb1.getValue();
            Integer value2 = (Integer) cb.getValue();
            System.out.println(value2);
            if(value!=null && value2!= null){
                juego.setNrocomputadora(value2);
                switch(value){
                case "Visible":
                    juego.setComputadorasVisibles(true);
                    System.out.println(juego.isComputadorasVisibles());
                    break;
                case "Oculta":   
                    juego.setComputadorasVisibles(false);
                    System.out.println(juego.isComputadorasVisibles());
                    break;}
                
                ViewVentanaPrincipal vp;
                try {
                    vp = new ViewVentanaPrincipal(juego);
                    btnGuardar.getScene().setRoot(vp.getRoot());
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ViewConfiguraciones.class.getName()).log(Level.SEVERE, null, ex);
                }
            
            }
            
            
        });
        btnRegresar.setOnAction(e->{
               
                try {
               ViewVentanaPrincipal ventp = new ViewVentanaPrincipal();
               btnRegresar.getScene().setRoot(ventp.getRoot());
               System.out.println("Entro a la ventana");
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ViewConfiguraciones.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        );
   
        

    }
    
}
