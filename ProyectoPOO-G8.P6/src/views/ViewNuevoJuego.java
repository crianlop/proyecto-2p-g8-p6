package views;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import model.Carta;
import threads.Juego;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import javafx.stage.Stage;
import model.Computadora;
import model.Jugador;
import model.MazoCartas;
import model.TipoJuego;
import proyectoPOOg8p6.Main;
import threads.ThreadGriton;
import threads.juegoTerminado;
import threads.mostrarMAL;

public class ViewNuevoJuego {

    private static Pane root;
    private Juego juego;
    private boolean loteria;
    private ArrayList<Stage> ventanascpus;
    private final Carta[][] cartas;

    public static Pane getRoot() {
        return root;}

    public void createContent() throws FileNotFoundException {
         
        root = new Pane();
        loteria = false;
        

        ventanascpus =  new ArrayList<Stage>();
       
        StackPane stackpane = new StackPane();
        GridPane tablero = new GridPane();
        GridPane t = new GridPane();
        
        ImageView cartaEnJuego = new ImageView();
        MazoCartas baraja = juego.getBaraja(); 
        juego.empezarJuego(cartaEnJuego,juego,ventanascpus,tablero);
        System.out.println(juego.getJugador().getTabla().getBaraja().getBaraja());
        Image image = new Image(new FileInputStream("src/data/images/background.jpg"));
        ImageView fondo = new ImageView(image);
        fondo.setFitWidth(640);
        fondo.setFitHeight(480);
        stackpane.getChildren().add(fondo);
        //root.getChildren().add(fondo);
        
        Rectangle rectangle = new Rectangle(325,450, Color.CYAN);
        rectangle.setArcWidth(20);
        rectangle.setArcHeight(20);
        stackpane.getChildren().add(rectangle);
        //root.getChildren().add(rectangle);
       
        Button btnLoteria = new Button("Loteria");
        btnLoteria.setOnAction(e->{
            if(!loteria){
                juegoTerminado loteria = new juegoTerminado(juego,juego.getJugador());
                loteria.start();         
            }
            
        });
        // LLENAR TABLAS
        
        
        
        VBox cntCarta = new VBox(25);
        if(juego.isComputadorasVisibles()){
            System.out.println(juego.getComputadoras());
            for(Computadora c:juego.getComputadoras()){
                Stage cpu = new Stage();
                GridPane tablacpu = new GridPane();
                llenarTablero(t, tablacpu,juego,c);
                Scene scenecpu = new Scene(tablacpu,255,435);
                scenecpu.setRoot(tablacpu);
                c.setGridpane(tablacpu);
                cpu.setTitle("OPONENTE: "+c.getNombre());
                cpu.setScene(scenecpu);
                cpu.show();
                ventanascpus.add(cpu);
            }
        }
                
        llenarTablero(t,tablero, juego, juego.getJugador());
        tablero.setAlignment(Pos.CENTER);
        t.setAlignment(Pos.TOP_RIGHT);
        cntCarta.setAlignment(Pos.CENTER_RIGHT);
        btnLoteria.setAlignment(Pos.CENTER);

        cntCarta.getChildren().addAll(cartaEnJuego, btnLoteria);        
        BorderPane todito = new BorderPane();
        todito.setLeft(t);
        todito.setCenter(tablero);
        todito.setRight(cntCarta);
        stackpane.getChildren().add(todito);
        //root.getChildren().add(todito);
        
            
        root.getChildren().add(stackpane);
        System.out.println(tablero.getChildren().size());
        ThreadGriton mainThread = new ThreadGriton(cartaEnJuego,juego,ventanascpus,tablero);
        mainThread.start();
 
    }
    public ViewNuevoJuego() {
        
        cartas = new Carta[4][4];
        loteria = false;
    }
    public ViewNuevoJuego(Juego juego) throws FileNotFoundException{
        this.juego = juego;
        cartas = juego.getJugador().getTabla().getTablacartas();
        createContent();
    }

    public Juego getJuego() {
        return juego;
    }

 

    public ArrayList<Stage> getVentanascpus() {
        return ventanascpus;
    }

    public void setVentanascpus(ArrayList<Stage> ventanascpus) {
        this.ventanascpus = ventanascpus;
    }

    public void llenarTablero(GridPane t ,GridPane tablero, Juego juego, Jugador jugador) {
        
        juego.setCartasRepartidas(false);
        tablero.setVgap(5);
        tablero.setHgap(5);
        t.setVgap(1);
        t.setHgap(1);
        
        
        while(!juego.isCartasRepartidas()){
        if(!juego.isCartasRepartidas()){
        for (int i=0; i < 4; i++) {
            for (int j=0; j<4;j++) {
                try {
                    StackPane stacked = new StackPane();
                    ImageView contenedor = new ImageView();
                    Image image = new Image(new FileInputStream("src/data/images/deck/"+jugador.getTabla().getTablacartas()[i][j].getId()+".png "));
                    contenedor.setImage(image);
                    contenedor.setFitWidth(60);
                    contenedor.setFitHeight(105);
                    stacked.getChildren().add(contenedor);
                    if(juego.getJugador().equals(jugador)){
                    addEvent(stacked);}
                    tablero.add(stacked, i, j);
                } catch (FileNotFoundException ex) {
                   System.out.println("ERROR");
                }
            } 
        }
        juego.setCartasRepartidas(true);
        } 
    }
        try {
            StackPane st = new StackPane();
            ImageView imV = new ImageView ();
            Label lbl1 = new Label(juego.toString().split(",")[1].split("=")[1]);
            Image imagenTipoJuego= new Image(new FileInputStream(new File("src/data/images/alineaciones/"+juego.getTipojuego().toString()+".png")));
            imV.setImage(imagenTipoJuego);
            imV.setFitHeight(100);
            imV.setFitWidth(100);
            st.getChildren().add(imV);
            t.add(st, 10, 200);
            t.add(lbl1, 10, 175);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ViewNuevoJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void addEvent(StackPane pane){
        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, event ->{ 
        Jugador jugador = juego.getJugador();
        Carta[][] cartas = jugador.getTabla().getTablacartas();
        
        if(juego.isEmpezoJuego()){    
        if (cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)].getId() ==  juego.getCartaEnJuego().getId() && !jugador.getTabla().getBaraja().getMapCartas().get(cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)])) {
           try {
              ImageView frijol = new ImageView(new Image(new FileInputStream(new File("src/data/images/frejol.png"))));
              frijol.setFitHeight(50);
              frijol.setFitWidth(50);
              pane.getChildren().add(frijol);
              jugador.getTabla().getBaraja().getMapCartas().replace(cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)],true);
              System.out.println(cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)]+";"+jugador.getTabla().getBaraja().getMapCartas().get(cartas[GridPane.getColumnIndex(pane)][GridPane.getRowIndex(pane)]));
          } catch (Exception ex) {
             System.out.println(ex.getMessage());
          }
      } 
        else{
            mostrarMAL thread = new mostrarMAL(pane);
            thread.start();
        }
    }       
}); 
        
}

    public boolean isLoteria() {
        return loteria;
    }

    public void setLoteria(boolean loteria) {
        this.loteria = loteria;
    }

    public static void setRoot(StackPane root) {
        ViewNuevoJuego.root = root;
    }

    
    
}
