/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import threads.Juego;

/**
 *
 * @author laptop
 */
public class ViewReportes {  
    private BorderPane root;
    private ArrayList<Juego> juegos;
    
    
    public ViewReportes(Juego juego){
        juegos = juego.getJuegos();
        createContent();
    }
    
    // ventana reportes, donde se crea el table view, tomando los datos del juego, mostrandolos ordenado por fecha.
    
    public void createContent(){
        String[] datos = {"fecha","duracion","nombre","nrocomputadora","tipojuego"};
        TableView tableview =new TableView();
        Collections.sort(juegos);   
        for(String campo:datos){
            TableColumn<String,Juego> column=new TableColumn<>(campo);
            column.setCellValueFactory(new PropertyValueFactory<>(campo.toLowerCase()));
            column.prefWidthProperty().bind(tableview.widthProperty().multiply(0.20));
            tableview.getColumns().add(column);
        }
        for(Juego j:juegos){
          tableview.getItems().add(j);
        }
       
    
    
    HBox contTitulo=new HBox(10);
    Label lbl=new Label("Reportes");
    contTitulo.getChildren().add(lbl);
    contTitulo.setAlignment(Pos.CENTER);
    root = new BorderPane();
    root.setTop(contTitulo);
    root.setCenter(tableview);
    Button btnVolver = new Button("Volver");
    btnVolver.setOnAction(e->{
        ViewVentanaPrincipal ventP;
            try {
                ventP = new ViewVentanaPrincipal();
                btnVolver.getScene().setRoot(ventP.getRoot());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ViewReportes.class.getName()).log(Level.SEVERE, null, ex);
            }
    });
    root.setBottom(btnVolver); 
        }

    public BorderPane getRoot() {
        return root;
    }
    
 
}
    

