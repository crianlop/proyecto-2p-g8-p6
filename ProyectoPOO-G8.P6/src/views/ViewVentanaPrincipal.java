package views;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;         
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import threads.Juego;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Jugador;
import model.TipoJuego;
import threads.ThreadGriton;

public class ViewVentanaPrincipal {

    private static StackPane root = new StackPane();
    private Juego juego;
    

    //creacion de la ventana principal del juego, su fondo, botones, etc

    public void createContent() throws FileNotFoundException {
        Image image = new Image(new FileInputStream("src/data/images/background.jpg"));
        ImageView fondo = new ImageView(image);
        fondo.setFitWidth(640);
        fondo.setFitHeight(480);
        Rectangle rectangle = new Rectangle(240,200, Color.YELLOW);
        rectangle.setArcWidth(20);
        rectangle.setArcHeight(20);
        Label label = new Label("Lotería Mexicana!");
        Button btnNuevoJuego = new Button("Nuevo Juego");
        Button btnConfiguraciones = new Button("Configuraciones");
        Button btnReportes = new Button("Reportes");
        VBox cntBotones = new VBox(15);
        cntBotones.setPadding(new Insets(20));
        cntBotones.setAlignment(Pos.CENTER);
        cntBotones.getChildren().addAll(label, btnNuevoJuego, btnConfiguraciones, btnReportes);
        root.setPadding(new Insets(20));
        root.getChildren().add(fondo);
        root.getChildren().add(rectangle);
        root.getChildren().add(cntBotones);

        btnNuevoJuego.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String nombre = inputTextAlert("Nuevo Juego", "Ingrese su nombre para empezar a jugar.", "Nombre:");
                System.out.println(nombre);
                
                if (nombre!=null&&!nombre.equals("") ) {
                    Alert info = new Alert(Alert.AlertType.INFORMATION);
                    info.setContentText("Intrucciones del juego: "+juego.getTipojuego());
                    info.showAndWait();
                    juego.getJugador().setNombre(nombre);
                    // validacion
                    ViewNuevoJuego ventanaJuego;
                    try {
                        ventanaJuego = new ViewNuevoJuego(juego);
                        btnNuevoJuego.getScene().setRoot(ventanaJuego.getRoot());
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ViewVentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            }
        });
        btnConfiguraciones.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            
                ViewConfiguraciones confg = new ViewConfiguraciones(juego);
                btnConfiguraciones.getScene().setRoot(confg.getRoot());

            }
        });
        btnReportes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ViewReportes reportes = new ViewReportes(juego);
                btnReportes.getScene().setRoot(reportes.getRoot());
        
            }
        });

    }

    public ViewVentanaPrincipal() throws FileNotFoundException{
        juego = new Juego();
        juego.setJugador(new Jugador());
        createContent();

    }
    public ViewVentanaPrincipal(Juego juego) throws FileNotFoundException{
        this.juego = juego;
        createContent();
    }
    public static StackPane getRoot() {return root;};

// verificacion si el usuario escribio un nombre o no 
    public String inputTextAlert(String title, String header, String content) {
        TextInputDialog alerta = new TextInputDialog();
        alerta.setTitle(title);
        alerta.setHeaderText(header);
        alerta.setContentText(content);
        Optional<String> nombre = alerta.showAndWait();
        if (nombre.isPresent()) {
            return nombre.get();
        }

        return null;
    }
 
}
